passgen
=======

Simple Python Script that Generates secure passwords for you. Default password length is 30 characters unless you specify a length at the command line using the "-l" or "--length" param

#### Examples:

`passgen --no-punctuation --length 32` 

Creates a password with only letters and numbers (no symbols) of length 32


`passgen --no-punctuation 32` 

Creates a password with only letters and numbers (no symbols) of length 32

`passgen -n 32` 

Creates a password with only letters and numbers (no symbols) of length 32

`passgen -l 32` 

Creates a password with symbols letters and numbers of length 32 

`passgen -n`

Creates a password with no symbols of length 30

`passgen`

Creates a password with symbols letters and numbers of length 30
